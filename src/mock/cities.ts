export const CITIES = [
  {
    id: 2968815,
    name: 'Paris',
    state: '',
    country: 'FR',
    coord: {
      lon: 2.3486,
      lat: 48.853401,
    },
  },
  {
    id: 3220968,
    name: 'Frankfurt am Main',
    state: '',
    country: 'DE',
    coord: {
      lon: 8.66361,
      lat: 50.130829,
    },
  },
  {
    id: 6359304,
    name: 'Madrid',
    state: '',
    country: 'ES',
    coord: {
      lon: -3.68275,
      lat: 40.489349,
    },
  },
  {
    id: 5245497,
    name: 'Berlin',
    state: 'WI',
    country: 'US',
    coord: {
      lon: -88.943451,
      lat: 43.96804,
    },
  },
  {
    id: 5637141,
    name: 'Amsterdam',
    state: 'MT',
    country: 'US',
    coord: {
      lon: -111.319962,
      lat: 45.75798,
    },
  },
];
