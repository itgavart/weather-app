import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  CityHourlyForecastInterface,
  CityWeatherInterface,
  Coord,
  WeatherServiceInterface,
} from './weather';
import { weatherTemperatureIconMap } from './weather-temperature-icon';
import { WeatherUnit } from './weather-unit';

@Injectable()
export class WeatherService implements WeatherServiceInterface {
  private temperatureIcon = weatherTemperatureIconMap[WeatherUnit.METRIC];

  constructor(private httpClient: HttpClient) {}

  public getTemperatureUnitIcon(): string {
    return this.temperatureIcon;
  }

  public getCityWeather$(coord: Coord): Observable<CityWeatherInterface> {
    const params = this.getRequestParams(coord);

    return this.httpClient.get<CityWeatherInterface>(
      `${environment.API}/data/2.5/weather`,
      {
        params,
      }
    );
  }

  public getCityHourlyForecast$(
    coord: Coord
  ): Observable<CityHourlyForecastInterface> {
    const params = this.getRequestParams(coord);

    return this.httpClient.get<CityHourlyForecastInterface>(
      `${environment.API}/data/2.5/forecast`,
      {
        params,
      }
    );
  }

  private getRequestParams({ lat, lon }: Coord): Record<string, string> {
    return {
      lat: lat.toString(),
      lon: lon.toString(),
      units: WeatherUnit.METRIC,
      appid: environment.OPEN_WEATHER_API_KEY || '',
    };
  }
}
