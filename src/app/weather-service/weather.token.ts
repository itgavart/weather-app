import { InjectionToken } from '@angular/core';
import { WeatherServiceInterface } from './weather';

export const WEATHER_SERVICE_TOKEN = new InjectionToken<WeatherServiceInterface>(
  'WEATHER_SERVICE_TOKEN'
);
