import { Observable } from 'rxjs';

export interface Coord {
  lon: number;
  lat: number;
}
export interface Wind {
  speed: number;
  deg: number;
}
export interface WeatherDetails {
  id: number;
  main: string;
  description: string;
  icon: string;
}
export interface Temperature {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}

export interface CityWeatherInterface {
  coord: Coord;
  weather: WeatherDetails[];
  base: string;
  main: Temperature;
  visibility: number;
  wind: Wind;
  id: number;
  name: string;
  dt: number;
  dt_txt: string;
}

export interface CityInterface {
  id: number;
  name: string;
  state?: string;
  country?: string;
  coord?: Coord;
}

export interface CityHourlyForecastInterface {
  cod: string;
  message: string;
  cnt: number;
  city: CityInterface;
  list: CityWeatherInterface[];
}

export interface WeatherServiceInterface {
  getTemperatureUnitIcon(): string;
  getCityWeather$(coord: Coord): Observable<CityWeatherInterface>;
  getCityHourlyForecast$(
    coord: Coord
  ): Observable<CityHourlyForecastInterface>;
}
