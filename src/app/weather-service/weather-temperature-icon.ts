import { WeatherUnit } from "./weather-unit";

export const weatherTemperatureIconMap = {
  [WeatherUnit.METRIC]: 'º',
};
