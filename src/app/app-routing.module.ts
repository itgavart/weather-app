import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'weather/:lat/:lon',
    loadChildren: () =>
      import('./weather-page/weather-page.module').then(
        (m) => m.WeatherPageModule
      ),
  },
  {
    path: 'hourly/:lat/:lon',
    loadChildren: () =>
      import('./hourly-weather-page/hourly-weather-page.module').then(
        (m) => m.HourlyWeatherPageModule
      ),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'weather',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
