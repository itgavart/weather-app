import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { filter, map, Observable, switchMap, take, tap } from 'rxjs';
import { CityServiceInterface } from '../city-service/city-service.interface';
import { CITY_SERVICE_TOKEN } from '../city-service/city-service.token';
import { WidgetWeatherInterface } from '../city-widget/widget-weather.inteface';
import {
  CityInterface,
  CityWeatherInterface,
  WeatherServiceInterface,
} from '../weather-service/weather';
import { WEATHER_SERVICE_TOKEN } from '../weather-service/weather.token';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherPageComponent implements OnInit {
  public cityWeather$!: Observable<WidgetWeatherInterface>;

  constructor(
    private activatedRote: ActivatedRoute,
    private router: Router,
    @Inject(CITY_SERVICE_TOKEN) private cityService: CityServiceInterface,
    @Inject(WEATHER_SERVICE_TOKEN)
    private weatherService: WeatherServiceInterface
  ) {}

  ngOnInit(): void {
    this.cityWeatherInit();
  }

  public moreDetails(): void {
    this.selectdCoords$()
      .pipe(take(1))
      .subscribe(({ lat, lon }: Params) =>
        this.router.navigate([`hourly/${lat}/${lon}`])
      );
  }

  private cityWeatherInit(): void {
    this.cityWeather$ = this.selectdCoords$().pipe(
      switchMap(({ lat, lon }: Params) =>
        this.weatherService.getCityWeather$({ lat, lon })
      ),
      map((cityWeather: CityWeatherInterface) => {
        const weather = cityWeather?.weather[0];
        const city = {
          id: cityWeather?.id,
          title: cityWeather?.name,
          icon: `https://openweathermap.org/img/w/${weather?.icon}.png`,
          temp: cityWeather?.main?.temp,
          temp_max: cityWeather?.main?.temp_max,
          temp_min: cityWeather?.main?.temp_min,
          wind_speed: cityWeather?.wind?.speed,
          description: weather.description,
          temp_unit_icon: this.weatherService.getTemperatureUnitIcon(),
        };

        return city;
      })
    );
  }

  private selectdCoords$(): Observable<Params> {
    return this.activatedRote.params.pipe(
      filter(({ lat, lon }: Params) => !!(lat && lon))
    );
  }
}
