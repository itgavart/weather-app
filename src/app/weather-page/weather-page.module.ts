import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherPageComponent } from './weather-page.component';
import { WeatherPageRoutingModule } from './weather-page-routing.module';
import { CityWidgetModule } from '../city-widget/city-widget.module';
import { MatRippleModule } from '@angular/material/core';

@NgModule({
  declarations: [WeatherPageComponent],
  imports: [
    CommonModule,
    WeatherPageRoutingModule,
    CityWidgetModule,
    MatRippleModule,
  ],
})
export class WeatherPageModule {}
