import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WEATHER_SERVICE_TOKEN } from './weather-service/weather.token';
import { WeatherService } from './weather-service/weather.service';
import { CITY_SERVICE_TOKEN } from './city-service/city-service.token';
import { CityService } from './city-service/city.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: WEATHER_SERVICE_TOKEN,
      useClass: WeatherService,
    },
    {
      provide: CITY_SERVICE_TOKEN,
      useClass: CityService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
