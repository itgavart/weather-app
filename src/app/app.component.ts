import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CITIES } from 'src/mock/cities';
import { getRandomIndex } from './helpers/random-index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private router: Router) {}
  ngOnInit(): void {
    const index = getRandomIndex(5);
    const initializedCity = CITIES[index];

    this.router.navigate([
      `weather/${initializedCity.coord.lat}/${initializedCity.coord.lon}`,
    ]);
  }
}
