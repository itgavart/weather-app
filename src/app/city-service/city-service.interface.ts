import { Observable } from 'rxjs';
import { CityInterface } from './city';

export interface CityServiceInterface {
  getCity$(id: number): Observable<CityInterface>;

  getCities$(): Observable<CityInterface[]>;
}
