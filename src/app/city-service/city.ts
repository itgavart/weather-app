import { CoordInterface } from './coord';

export interface CityInterface {
  id: number;
  name: string;
  state?: string;
  country?: string;
  coord?: CoordInterface;
}
