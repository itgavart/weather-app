import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, map, Observable, of } from 'rxjs';
import { CITIES } from 'src/mock/cities';
import { CityInterface } from './city';
import { CityServiceInterface } from './city-service.interface';

@Injectable()
export class CityService implements CityServiceInterface {
  private selectedCity$ = new BehaviorSubject<CityInterface>(CITIES[0]);

  public getSelectedCity(): Observable<CityInterface> {
    return this.selectedCity$.asObservable();
  }

  public setCity(city: CityInterface): void {
    this.selectedCity$.next(city);
  }

  public getCity$(id: number): Observable<CityInterface> {
    return this.getCities$().pipe(
      map(
        (cities: CityInterface[]) =>
          cities.find((city) => id == city.id) as CityInterface
      ),
      filter((city: CityInterface) => !!city)
    );
  }

  public getCities$(): Observable<CityInterface[]> {
    return of(CITIES);
  }
}
