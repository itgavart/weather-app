import { InjectionToken } from '@angular/core';
import { CityServiceInterface } from './city-service.interface';

export const CITY_SERVICE_TOKEN = new InjectionToken<CityServiceInterface>(
  'CITY_SERVICE_TOKEN'
);
