import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { WidgetWeatherInterface } from './widget-weather.inteface';

@Component({
  selector: 'app-city-widget',
  templateUrl: './city-widget.component.html',
  styleUrls: ['./city-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CityWidgetComponent {
  @Input() cityWeather!: WidgetWeatherInterface;
}
