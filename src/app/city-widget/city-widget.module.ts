import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CityWidgetComponent } from './city-widget.component';

@NgModule({
  declarations: [CityWidgetComponent],
  imports: [CommonModule],
  exports: [CityWidgetComponent],
})
export class CityWidgetModule {}
