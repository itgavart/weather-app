export interface WidgetWeatherInterface {
  id: number;
  title: string;
  icon: string;
  temp: number;
  temp_max: number;
  temp_min: number;
  wind_speed: number;
  description: string;
  temp_unit_icon: string;
}
