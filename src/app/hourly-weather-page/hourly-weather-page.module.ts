import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HourlyWeatherPageComponent } from './hourly-weather-page.component';
import { HourlyWeatherPageRoutingModule } from './hourly-weather-page-routing.module';
import { HourlyWidgetModule } from '../hourly-widget/hourly-widget.module';

@NgModule({
  declarations: [HourlyWeatherPageComponent],
  imports: [CommonModule, HourlyWeatherPageRoutingModule, HourlyWidgetModule, ],
})
export class HourlyWeatherPageModule {}
