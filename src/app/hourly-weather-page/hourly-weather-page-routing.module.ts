import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HourlyWeatherPageComponent } from './hourly-weather-page.component';

const routes: Routes = [
  {
    path: '',
    component: HourlyWeatherPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HourlyWeatherPageRoutingModule {}
