import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Inject,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { filter, map, Observable, switchMap, tap } from 'rxjs';
import { CityServiceInterface } from '../city-service/city-service.interface';
import { CITY_SERVICE_TOKEN } from '../city-service/city-service.token';
import { WidgetWeatherInterface } from '../city-widget/widget-weather.inteface';
import {
  CityHourlyForecastInterface,
  CityWeatherInterface,
  Coord,
  WeatherServiceInterface,
} from '../weather-service/weather';
import { WEATHER_SERVICE_TOKEN } from '../weather-service/weather.token';

@Component({
  selector: 'app-hourly-weather-page',
  templateUrl: './hourly-weather-page.component.html',
  styleUrls: ['./hourly-weather-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HourlyWeatherPageComponent implements OnInit {
  public hourlyWeather$!: Observable<WidgetWeatherInterface[]>;
  public cityName!: string;

  constructor(
    private activatedRote: ActivatedRoute,
    @Inject(CITY_SERVICE_TOKEN) private cityService: CityServiceInterface,
    @Inject(WEATHER_SERVICE_TOKEN)
    private weatherService: WeatherServiceInterface
  ) {}

  ngOnInit(): void {
    this.hourlyWeatherInit();
  }

  private hourlyWeatherInit(): void {
    this.hourlyWeather$ = this.selectedCoords$().pipe(
      switchMap(({ lat, lon }: Params) => this.getHourlyWeather$({ lat, lon }))
    );
  }

  private getHourlyWeather$(
    coord: Coord
  ): Observable<WidgetWeatherInterface[]> {
    return this.selectedCoords$().pipe(
      switchMap(({ lat, lon }: Params) =>
        this.weatherService.getCityHourlyForecast$({ lat, lon })
      ),
      tap(
        (weather: CityHourlyForecastInterface) =>
          (this.cityName = weather?.city?.name)
      ),
      map((hourlyWeather: CityHourlyForecastInterface) => {
        return hourlyWeather.list.map((hourly: CityWeatherInterface) => {
          const weather = hourly?.weather[0];
          const date = new Date(hourly?.dt_txt);
          const time = `Time ${date.getHours()}:${date.getMinutes()}`;
          const city = {
            id: hourly?.id,
            title: time,
            icon: `https://openweathermap.org/img/w/${weather?.icon}.png`,
            temp: hourly?.main?.temp,
            temp_max: hourly?.main?.temp_max,
            temp_min: hourly?.main?.temp_min,
            wind_speed: hourly?.wind?.speed,
            description: weather.description,
            temp_unit_icon: this.weatherService.getTemperatureUnitIcon(),
          };

          return city;
        });
      })
    );
  }

  private selectedCoords$(): Observable<Params> {
    return this.activatedRote.params.pipe(
      filter(({ lat, lon }: Params) => !!(lat && lon))
    );
  }
}
