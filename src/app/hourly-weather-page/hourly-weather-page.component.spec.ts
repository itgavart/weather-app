import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HourlyWeatherPageComponent } from './hourly-weather-page.component';

describe('HourlyWeatherPageComponent', () => {
  let component: HourlyWeatherPageComponent;
  let fixture: ComponentFixture<HourlyWeatherPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HourlyWeatherPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HourlyWeatherPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
