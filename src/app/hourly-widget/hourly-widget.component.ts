import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { CityInterface } from '../city-service/city';
import { WidgetWeatherInterface } from '../city-widget/widget-weather.inteface';

@Component({
  selector: 'app-hourly-widget',
  templateUrl: './hourly-widget.component.html',
  styleUrls: ['./hourly-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HourlyWidgetComponent {
  @Input() weather!: WidgetWeatherInterface;
}
