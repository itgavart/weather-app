import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HourlyWidgetComponent } from './hourly-widget.component';

@NgModule({
  declarations: [HourlyWidgetComponent],
  imports: [CommonModule],
  exports: [HourlyWidgetComponent],
})
export class HourlyWidgetModule {}
