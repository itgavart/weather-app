import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HourlyWidgetComponent } from './hourly-widget.component';

describe('HourlyWidgetComponent', () => {
  let component: HourlyWidgetComponent;
  let fixture: ComponentFixture<HourlyWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HourlyWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HourlyWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
