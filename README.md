# The Weather App

There is not enough time to think over the application well.
Everything can be made much more interesting and practical.
When the application is initialized, a city is randomly selected.
Created 2 pages, the first one displays the current weather in the city, the second one displays the hourly weather.
To display temperatures, 2 widgets are used that have one data model.
One is large, the other is list oriented.
These components receive data via input, for ease of reuse.
All services are injected by a token with a specific interface.
Components for displaying weather are separated into modules and can be very easily transferred to libraries for reuse in other projects.
Tokens, interfaces can also be moved to libraries if necessary.
Nothing unusual, simple solutions and you can make it even more interesting.

## Mockup
there is a drawn mockup `./mockup.jpg`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
